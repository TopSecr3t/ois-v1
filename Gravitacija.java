import java.util.Scanner;

public class Gravitacija{
    
    static double c = 6.674e-11;
    static double m = 5.972e24;
    static double r = 6.371e6;
    
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        
        int nv = sc.nextInt();
        izpis(nv, izracun(nv));
    }
    
    public static double izracun(double nv) {
        double a = (c*m)/((r+nv)*(r+nv));
        return a;
    }
   public static void izpis(int nad_vis, double grav_pos) {
		System.out.println("Nadmorska visina: " + nad_vis);
		System.out.println("Gravitacijski pospesek: " + grav_pos);
	} 
}
